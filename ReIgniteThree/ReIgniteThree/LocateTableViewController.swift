//
//  LocateTableViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/6/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class LocateTableViewController: UITableViewController {
    
    var allUsers:NSArray = []
    var currentUser = PFUser()
    var recipient:NSMutableArray = []
    var spouseRelation:PFRelation = PFRelation()
    var spouse:NSArray = []
    var spouseConnectionReply:PFObject!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let query = PFUser.query()
        query?.orderByAscending("username")
        query?.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.allUsers = objects!
                //print(self.allUsers)
                self.tableView.reloadData()
            }
        })

        currentUser = PFUser.currentUser()!
       
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allUsers.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)

        // Configure the cell...
        let user:PFUser = allUsers.objectAtIndex(indexPath.row) as! PFUser
        cell.textLabel?.text = user.username

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        let cell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        let ct = UITableViewCellAccessoryType.Checkmark
        cell.accessoryType = ct
        let user:PFUser = allUsers.objectAtIndex(indexPath.row) as! PFUser
        let spouseRelation:PFRelation = self.currentUser.relationForKey("spouseRelation")
        spouseRelation.addObject(user)
        
        currentUser.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            
            if success {
                print("successful relation")
                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                self.presentViewController(vc, animated: true, completion: nil)
                /*
                //send request to recipient
                let spouseConnectionReply = PFObject(className: "SpouseReply")
                spouseConnectionReply["connection"] = "connections"
                spouseConnectionReply.setObject(<#T##object: AnyObject##AnyObject#>, forKey: <#T##String#>)
                */
                
                self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                
              
                
                let query = spouseRelation.query()
                query.orderByAscending("username")
                query.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                    
                    if error != nil{
                        print(error?.localizedDescription)
                    } else {
                        self.spouse = objects!
                        
                    }
                    
                    let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                    spouseRelation.addObject(user)
                    self.recipient.addObject(user.objectId!)
                    
                    self.sendRelationRequest()
                })

            }
        }

        
    }
    
    func sendRelationRequest(){
       
        spouseConnectionReply = PFObject(className: "SpouseReply")
        spouseConnectionReply["connection"] = "connections"
        spouseConnectionReply["connectionStatus1"] = "accept"
        spouseConnectionReply["connectionStatus2"] = "decline"
        spouseConnectionReply.setObject(recipient, forKey: "recipientsIds")
        spouseConnectionReply.setObject(PFUser.currentUser()!, forKey: "senderId")
        spouseConnectionReply.setObject((PFUser.currentUser()?.username)!, forKey: "senderName")
        
        spouseConnectionReply.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            
            if success{
               
            } else {
                //alert bad connection
                print(error?.localizedDescription)
            }
        }
    }

    func cutRelationship(){
        if spouseConnectionReply != nil{
            spouseConnectionReply.deleteInBackground()
        } else {
            print("no spouse")
        }
       
    }
    


}
