//
//  MessageInboxViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/13/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
import AVKit
import AVFoundation

class MessageInboxViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var allUsers:NSArray = []
    var image:UIImage = UIImage()
    var session:NSTimer!
    let imagePicker:UIImagePickerController = UIImagePickerController()
    var videoFilePath:String!
    var spouseRelation:PFRelation = PFRelation()
    var messages:NSArray = []
    var selectedMessage:PFObject!
    var moviePlayer:AVPlayerViewController!
    var messageSession:NSTimer!

    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
        }
       
        tableView.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let query:PFQuery = PFQuery(className:"Messages")
        query.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        query.orderByDescending("createdAt")
        query.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
        
        if error != nil{
        print(error?.localizedDescription)
        } else{
        //we have a message
        self.messages = objects!
            if self.messages == []{
                //alert no messages
                //send back to main page
                let alertController = UIAlertController(title: "No messages have been sent", message: "Check back soon", preferredStyle: UIAlertControllerStyle.Alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                    self.presentViewController(vc, animated: true, completion: nil)
                    
                })
                alertController.addAction(defaultAction)
                self.presentViewController(alertController, animated: true, completion: nil)
                
            } else {
                print("inbox has messages")
                
            }
        print(self.messages.count)
        
        self.tableView.reloadData()
        }
        }
        let me = PFUser.currentUser()?.objectId
        print(me)

        
    }
    


  
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        
        // Configure the cell...
        let message:PFObject = messages.objectAtIndex(indexPath.row) as! PFObject
        cell.textLabel?.text = message.objectForKey("senderName") as? String
        
        let file:NSString! = message.objectForKey("fileType") as! NSString
        if file.isEqualToString("image"){
        cell.imageView?.image = UIImage(named: "icon_image")
        } else {
        cell.imageView?.image = UIImage(named: "icon_video")
        }
        

        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selectedMessage = messages.objectAtIndex(indexPath.row) as! PFObject
        
        
        let file:NSString! = selectedMessage.objectForKey("fileType") as! NSString
        if file.isEqualToString("image"){
        
        performSegueWithIdentifier("showImage", sender: self)
        } else {
        
        let videoFile:PFFile = selectedMessage.objectForKey("imageFile") as! PFFile
        let url = NSURL(string: videoFile.url!)
        // var url:NSURL = NSURL(string: videoFile.url!)!
        let player = AVPlayer(URL: url!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.addChildViewController(playerController)
        self.view.addSubview(playerController.view)
        playerController.view.frame = self.view.frame
        
        player.play()
        
        destroyVideo()
        
        }
        /*
        var recipientIds:NSMutableArray = []
        recipientIds = selectedMessage.objectForKey("recipientsIds") as! NSMutableArray
        print(recipientIds)
        
        selectedMessage.deleteInBackground()
        recipientIds.removeAllObjects()
        selectedMessage.setObject(recipientIds, forKey: "recipientsIds")
        selectedMessage.saveInBackground()
        */
        destroyVideo()
        

        
    }
    
    func destroyVideo(){
        messageSession = NSTimer.scheduledTimerWithTimeInterval(6.0, target: self, selector: #selector(MessageInboxViewController.disposeVideo), userInfo: nil, repeats: false)
    }
    
    func stopTimer(){
        messageSession.invalidate()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return messages.count
    }


    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showImage"{
            
            var ivc = ImageViewController()
            ivc = segue.destinationViewController as! ImageViewController
            ivc.message = selectedMessage
            
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        //stopTimer()
    }
    
    func disposeVideo(){
        
        var recipientIds:NSMutableArray = []
        recipientIds = selectedMessage.objectForKey("recipientsIds") as! NSMutableArray
        print(recipientIds)
        
        selectedMessage.deleteInBackground()
        recipientIds.removeAllObjects()
        selectedMessage.setObject(recipientIds, forKey: "recipientsIds")
        selectedMessage.saveInBackground()
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
        
    }
   

}
