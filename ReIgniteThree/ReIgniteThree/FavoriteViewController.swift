//
//  FavoriteViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/23/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse


class FavoriteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    var name:String!
    var number:String!
    var address:String!
    var lat:String!
    var lng:String!
    var placesArray:[Place] = []
    var place:NSArray = []
    var splaceArray:[Place] = []

    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
            
        }
        
        //tableView.delegate = self
        
        /*
        //(myNlat as NSString).doubleValue
        print(name)
        print(number)
        print(address)
        
        let place = Place()
        place.name = name
        place.phoneNumber = number
        place.address = address
       // place.lat = (lat as NSString).doubleValue
       // place.lng = (lng as NSString).doubleValue
        
        placesArray.append(place)
        */
        let query = PFQuery(className: "Favorite")
        query.orderByDescending("createdAt")
        query.whereKey("cUser", containsString: PFUser.currentUser()?.username)
        query.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) in
            
          
            if objects != nil{
                
                self.place = objects!
                   self.tableView.reloadData()
                
            } else {
                print(error?.localizedDescription)
            }
        }
    

     
        
    }



    @IBAction func backButtonTapped(sender: AnyObject) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! FavoriteTableViewCell
        print(place.count)
        let coolPlace = place[indexPath.row]
        /*
        print(placesArray.count)
        let thePlaces = placesArray[indexPath.row]
        print(thePlaces.name)
        */
        
        
        cell.name.text = coolPlace.objectForKey("name") as? String
        cell.address.text = coolPlace.objectForKey("address") as? String
        cell.number.text = coolPlace.objectForKey("number") as? String
        cell.cosmos.rating = (coolPlace.objectForKey("rating") as? Double)!
        cell.open.text = coolPlace.objectForKey("availability") as? String
        cell.imageView?.image = UIImage(data: (coolPlace.objectForKey("imgData") as? NSData)!)
        
        
        
        
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(place.count)
        
        let thePlaces = place[indexPath.row]
        //print(placesArray.count)
        
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("FavoriteDetailViewController") as! FavoriteDetailViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let splace = Place()
        controller.name = thePlaces.objectForKey("name") as? String
        controller.address = thePlaces.objectForKey("address") as? String
        controller.number = thePlaces.objectForKey("number") as? String
        controller.lat = thePlaces.objectForKey("lat") as? String
        controller.lng = thePlaces.objectForKey("lng") as? String
        controller.open = thePlaces.objectForKey("availability") as? String
        controller.rating = thePlaces.objectForKey("rating") as? Double!
        splace.name = (thePlaces.objectForKey("name") as? String)!
        splace.address = (thePlaces.objectForKey("address") as? String)!
        splace.phoneNumber = (thePlaces.objectForKey("number") as? String)!
        splace.lat = (thePlaces.objectForKey("lat"))!.doubleValue
        splaceArray.append(splace)
        appDelegate.window?.rootViewController = controllerNav

        
        
    }
    /*
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        let thePlaces = place[indexPath.row]
        if editingStyle == UITableViewCellEditingStyle.Delete {
          
            splaceArray.removeAtIndex(thePlaces.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
        /*
        let objectDel = PFObject(className: "Favorite")
        objectDel.removeObjectForKey(thePlaces.objectForKey("name") as? String)
        objectDel.removeObjectForKey(self.theAddress)
        objectDel.removeObjectForKey(self.theNumber)
        objectDel.removeObjectForKey(self.theLat)
        objectDel.removeObjectForKey(self.theLng)
        objectDel.saveInBackground()
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("FavSWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
        */

        let objectDel = PFObject(className: "Favorite")
        objectDel.removeObjectForKey((thePlaces.objectForKey("name") as? String)!)
        objectDel.removeObjectForKey((thePlaces.objectForKey("address") as? String)!)
        objectDel.removeObjectForKey((thePlaces.objectForKey("number") as? String)!)
        objectDel.removeObjectForKey((thePlaces.objectForKey("lat") as? String)!)
        objectDel.removeObjectForKey((thePlaces.objectForKey("lng") as? String)!)
        
    }
    */
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return place.count
    }
   }
