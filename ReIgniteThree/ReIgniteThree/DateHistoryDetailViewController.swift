//
//  DateHistoryDetailViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/14/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class DateHistoryDetailViewController: UIViewController {

    
    @IBOutlet var date: UILabel!
    @IBOutlet var time: UILabel!
    @IBOutlet var attire: UILabel!
    @IBOutlet var location: UILabel!
    @IBOutlet var sender: UILabel!
    var theDate:String!
    var theTime:String!
    var theAttire:String!
    var theLocation:String!
    var theSender:String!
    var spouseRelation:PFRelation = PFRelation()
    var recipient:NSMutableArray = []
    var spouse:NSArray = []
    var currentUser:PFUser = PFUser()
    var counter = 0
    var someArray:NSArray = []
    var theIndex:Int!
    var random:Int?
    var user:PFUser!
    var finalRandomNumber:Int!
    var recName:String?
    var receiverName:String?
    @IBOutlet var response: UITextField!
    @IBOutlet var acceptButton: UIButton!
    
    var newRandom:Int?
    var rRandom:Int?
    var theConfirmedNumber:Int?
    var theRandomNumber:Int?
    var theRecName:String?
    var userObjId:String!
    var theConfirmedDate:String?
    var theDateStamp:String?
    var theDateComments:String?
 
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //acceptButton.enabled = false
        //acceptButton.tintColor = UIColor.grayColor()
        
        self.hideKeyboardWhenTappedAround()
        
         spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
        
        if let nav = navigationController?.navigationBar{
          
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.grayColor()]
            
        }
        
        let checkDateRequest = PFQuery(className: "DateRequest")
        checkDateRequest.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
            
            if objects != nil{
                
                for object in objects!{
                    
                    //self.objectIds.append((object.objectId! as String?)!)
                    let me = object.objectId! as String!
                    self.userObjId = me
                    print(self.userObjId)
                }
                
                
                
            } else {
                
            }
        })
        

        
        
        let query = spouseRelation.query()
        query.orderByAscending("username")
        query.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.spouse = objects!
                
            }
            
            self.user = self.spouse.objectAtIndex(0) as! PFUser
            print(self.user.username)
            
            let aQuery = PFQuery(className: "DateRequest")
            //aQuery.whereKey("finalStatus", containsString: "accepted")
            //aQuery.whereKey("recUsername", containsString: PFUser.currentUser()?.username)
            //aQuery.whereKey("random", containsString: "confirmedNumber")
            aQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) in
                
                if objects != nil{
                    print(objects)
                    
                    for object in objects!{
                        
                      
                        
                        let num:Int? = object.objectForKey("confirmedNumber") as? Int
                         self.recName = object.objectForKey("recUsername") as? String
                        self.random = object.objectForKey("randomNumber") as? Int
                        let finalStatus = object.objectForKey("finalStatus") as? String
                        let cDate = object.objectForKey("datePicker") as? String
                        //let dateComments = object.objectForKey("dateComments") as? String
                        let dressType = object.objectForKey("dressType") as? String
                        let local = object.objectForKey("location") as? String
                        //let confirmedComments = object.objectForKey("confirmedComments") as? String
                        //let confirmedDate = object.objectForKey("confirmedDate") as? String
                        let confirmedDressType = object.objectForKey("confirmedDressType") as? String
                        let confirmedLocation = object.objectForKey("confirmedLocation") as? String
                        var dateStamp = object.objectForKey("dateStamp") as? String
                        var confirmedDateStamp = object.objectForKey("confirmedTimeStamp") as? String
                        print(dateStamp)
                        /*
                        if dateComments == nil{
                            print("comments nil")
                        } else if let myDateComments = dateComments {
                            self.theDateComments = myDateComments
                        } else {
                            print("no comments")
                        }
                        */
                        if dateStamp == nil{
                            dateStamp = "1"
                           
                        } else if let mydateStamp = dateStamp{
                            dateStamp = mydateStamp
                        } else {
                            print("no datestamp")
                        }
                        
                        if confirmedDateStamp == nil{
                            confirmedDateStamp = "2"
                            
                        } else if let myConfirmedDateStamp = confirmedDateStamp{
                            confirmedDateStamp = myConfirmedDateStamp
                        } else {
                            print("no confirm datestamp")
                        }
                        print(confirmedDateStamp)
                        self.theDateStamp = dateStamp
                        print(dateStamp)
                        if dateStamp == confirmedDateStamp {
                            print(dateStamp)
                            print(confirmedDateStamp)
                        } else {
                            print("no match")
                        }
                        if cDate == nil{
                            print("cDate is nil")
                        } else if let myCDate = cDate{
                            print(myCDate)
                            self.theConfirmedDate = myCDate
                        } else {
                            print("no date")
                        }
                        if num == nil{
                            print("nil confirmed number")
                        } else if let myConfirmedNum = num{
                            print(myConfirmedNum)
                            self.theConfirmedNumber = myConfirmedNum
                           
                          
                        } else {
                            print("no num")
                        }
                       
                       
                        if self.random == nil{
                            print("nil random number")
                        } else if let myRandom = self.random{
                            print(myRandom)
                            self.theRandomNumber = myRandom
                        } else {
                            print("no numbers")
                        }
                        
                        if self.recName == nil{
                            print("nil name")
                        } else if let newRecName = self.recName {
                            print(newRecName)
                            self.theRecName = newRecName
                            
                        } else {
                            print("no name")
                        }
                        if self.theRandomNumber == self.theConfirmedNumber{
                            print("number")
                        } else {
                            print("no number")
                        }
                       
                        if finalStatus == "accepted"{
                            print("has been accepted")
                        }
                        
                        if dateStamp == confirmedDateStamp{
                            print("match on date")
                        }
                        if dressType == confirmedDressType{
                            print("dress type")
                        }
                        if local == confirmedLocation{
                            print("local")
                        }
                                               let dateQuery = PFQuery(className: "DateRequest")
                        //dateQuery.whereKey("objectId", containsString: self.userObjId)
                        
                        dateQuery.whereKey("status", containsString: "accepted")
                        //dateQuery.whereKey("senderName", containsString: PFUser.currentUser()?.username)
                        //dateQuery.whereKey("confirmedDate", equalTo: self.theConfirmedDate!)
                        dateQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) in
                            
                            if objects != nil{
                            print(objects)
                                if objects! == [] {
                                    print("empty array")
                                    //self.acceptButton.enabled = true
                                    //self.acceptButton.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
                                } else {
                                   print("filled array")
                                    
                                    //self.acceptButton.enabled = false
                                    //self.acceptButton.backgroundColor = UIColor.grayColor()
                                }
                                
                            } else {
                                print(error?.localizedDescription)
                            }
                        }
                    }
                    
                    if objects! == [] {
                        
                    } else {
                        
                    }
                } else {
                    print(error?.localizedDescription)
                }
                
            }

            //self.recipient.addObject(user.objectId!)
        })
        
 
        /*
       let acceptedDateQuery = PFQuery(className: "DateRequest")
        
        //acceptedDateQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        acceptedDateQuery.orderByDescending("createdAt")
        //acceptedDateQuery.whereKey("senderName", equalTo: (user)
        //acceptedDateQuery.whereKey("senderName", equalTo: user.username!)
       // acceptedDateQuery.whereKey("status", containsString: "accepted")
        //acceptedDateQuery.whereKey("status", notEqualTo: "accepted")
        acceptedDateQuery.whereKey("recUsername", containsString: PFUser.currentUser()?.username)
        acceptedDateQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
        
            for object in objects!{
                if let newRandom:Int? = object.objectForKey("acceptedRandom") as? Int{
                    
                    print(newRandom)
                    if newRandom == nil{
                        print("no numbers yet still nil")
                    } else if newRandom <= 1000{
                        
                            //enable button
                            self.acceptButton.enabled = true
                            self.acceptButton.tintColor = UIColor.blueColor()
                        } else {
                            //disable button
                            self.acceptButton.enabled = false
                            self.acceptButton.tintColor = UIColor.grayColor()
                        }

                
                    
                    
                } else {
                    print("no newRandom")
                    self.acceptButton.enabled = true
                    self.acceptButton.tintColor = UIColor.blueColor()
                }
                self.acceptButton.enabled = true
                self.acceptButton.tintColor = UIColor.blueColor()
                
            }
            print(objects)
           
            
        }
        */
        
        
           
        //put optional in to mae sure data is avaiable for date/time/attire/location
       print(theDate)
        
        date.text = theDate
        //time.text = theTime
        attire.text = theAttire
        location.text = theLocation
        
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }

    @IBAction func acceptedResponse(sender: AnyObject) {
        
   
        if response.text?.characters.count <= 1{
            //alert please send your baby a comment
            let alertController = UIAlertController(title: "Username and/or password is incorrect", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
              self.response.text = ""
            })
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            print("theres enough characters")
            let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
            self.recipient.addObject(user.objectId!)
            let comments = response.text
            //counter = 1
            let dateRequest = PFObject(className:"AcceptedDateRequest")
            //dateRequest["counter"] = counter
            dateRequest["status"] = "accepted"
            // dateRequest["finalStatus"] = "accepted"
            dateRequest["dateComments"] = comments
            //dateRequest["acceptedRandom"] = random
            dateRequest["confirmedDate"] = theDate
            dateRequest["confirmedDressType"] = theAttire
            dateRequest["confirmedLocation"] = theLocation
            dateRequest["confirmedComments"] = comments
            dateRequest["dateOperation"] = "completed"
            dateRequest["confirmedNumber"] = self.theRandomNumber
            //dateRequest["recUsername"] = self.recName
            dateRequest["conditional"] = false
            dateRequest["confirmedTimeStamp"] = self.theDateStamp
            dateRequest.setObject(recipient, forKey: "recipientsIds")
            dateRequest.setObject(PFUser.currentUser()!, forKey: "senderId")
            dateRequest.setObject((PFUser.currentUser()?.username)!, forKey: "senderName")
            
            dateRequest.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
                
                if success{
                    
                    print(self.theDateStamp)
                    print(comments)
                    print(self.theAttire)
                    print(self.theRecName)
                    
                    
                    let query = PFQuery(className: "DateRequest")
                    //query.whereKey("dateStamp", equalTo: self.theDateStamp!)
                    //query.whereKey("comments", equalTo: comments!)
                    query.whereKey("dressType", equalTo: self.theAttire!)
                    //query.whereKey("recUsername", equalTo: (self.theRecName)!)
                    //query.whereKey("senderId", equalTo: (PFUser.currentUser()?.username!)!)
                    //query.whereKey("senderName", equalTo: (PFUser.currentUser()?.username!)!)
                    query.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
                        
                        for object in objects!{
                            object.deleteInBackground()
                            
                            let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                            let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                            self.presentViewController(vc, animated: true, completion: nil)
                        }
                    })
                    
                } else {
                    print(error?.localizedDescription)
                }
                
                
            }

        }
        
     
    }
    
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
    


}
