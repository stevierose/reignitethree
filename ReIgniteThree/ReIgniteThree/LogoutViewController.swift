//
//  LogoutViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/19/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class LogoutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        PFUser.logOut()
        
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
        

    }

 
}
