//
//  VideoTableViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/7/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Foundation
import AssetsLibrary
import Parse

class VideoTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var allUsers:NSArray = []
    var currentUser = PFUser()
    var image:UIImage? = nil
    let imagePicker:UIImagePickerController = UIImagePickerController()
    var videoFilePath:String!
    var spouseRelation:PFRelation = PFRelation()
    let spouses:NSArray = []
    var recipients:NSMutableArray = []
    var counter = 0
    var messages:NSArray = []
    var user:PFUser = PFUser()
    var mySpouseArray:NSArray = []
    var acceptedArray:NSArray = []
    var spouse:NSArray = []
    var recipient:NSMutableArray = []
    var isCamera:Bool! = false
    var isPhotoLibrary:Bool! = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.grayColor()]
            
            
        }
        
        let fQuery:PFQuery = PFQuery(className: "SpouseReply")
        //fQuery.whereKey("other", containsString: PFUser.currentUser()?.username)
        fQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) in
            
            if objects != nil {
                
            }
            
        }
        
        
        
        
        print("name doesnt match")
        
        
        let spQuery:PFQuery = PFQuery(className: "SpouseReply")
        spQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        //sQuery.whereKey("recUser", containsString: PFUser.currentUser()?.username)
        spQuery.orderByAscending("createdAt")
        spQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if objects != nil {
                
                
                self.mySpouseArray = objects!
                print(self.mySpouseArray.count)
                print(self.mySpouseArray)
                
                
                
                if self.mySpouseArray.count >= 1 {
                    //cool to enter initialize spouseRelation
                    self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                    let squery = self.spouseRelation.query()
                    squery.orderByAscending("username")
                    squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                        
                        if error != nil{
                            print(error?.localizedDescription)
                        } else {
                            self.spouse = objects!
                            
                            self.image = UIImage()
                            self.imagePicker.delegate = self
                            self.imagePicker.allowsEditing = false
                            //imagePicker.sourceType = .Camera
                            self.imagePicker.videoMaximumDuration = 6.0
                            
                            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                                
                                self.isCamera = true
                                self.imagePicker.sourceType = .Camera
                                
                            } else if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
                            
                                self.isPhotoLibrary = true
                                self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                            
                            }
                             else {
                                
                                //put alert in here
                                let alertController = UIAlertController(title: "No images available", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                    
                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                                    self.presentViewController(vc, animated: true, completion: nil)
                                    
                                })
                                alertController.addAction(defaultAction)
                                self.presentViewController(alertController, animated: true, completion: nil)
                            }
                            
                            
                            if self.isPhotoLibrary == true || self.isCamera == true{
                               
                                self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(self.imagePicker.sourceType)!
                                
                                self.presentViewController(self.imagePicker, animated: false, completion: nil)
                                
                
                            } else {
                                print("no image available")
                                let alertController = UIAlertController(title: "No images available", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                 
                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                                    self.presentViewController(vc, animated: true, completion: nil)
                                    
                                })
                                alertController.addAction(defaultAction)
                                self.presentViewController(alertController, animated: true, completion: nil)
                            }
                            
                            
                        }
                        
                        let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                        
                        if user.objectId != nil{
                            //have a user hide connection button
                            
                            
                        } else {
                            
                        }
                        
                        self.recipient.addObject(user.objectId!)
                    })
                    
                } else {
                    //check if you have an accepted connection
                    
                    let aQuery:PFQuery = PFQuery(className: "AcceptedConnection")
                    aQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
                    aQuery.orderByAscending("createdAt")
                    aQuery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
                        
                        if objects != nil {
                            self.acceptedArray = objects!
                            if self.acceptedArray.count >= 1{
                                //yay they can do their thang
                                
                                self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                                let squery = self.spouseRelation.query()
                                squery.orderByAscending("username")
                                squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                                    
                                    if error != nil{
                                        print(error?.localizedDescription)
                                    } else {
                                        self.spouse = objects!
                                        
                                        self.image = UIImage()
                                        self.imagePicker.delegate = self
                                        self.imagePicker.allowsEditing = false
                                        //imagePicker.sourceType = .Camera
                                        self.imagePicker.videoMaximumDuration = 6.0
                                        
                                        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                                            
                                            self.imagePicker.sourceType = .Camera
                                            
                                        } else {
                                            
                                            self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                                        }
                                        self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(self.imagePicker.sourceType)!
                                        
                                        self.presentViewController(self.imagePicker, animated: false, completion: nil)
                                    }
                                    
                                    let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                                    
                                    if user.objectId != nil{
                                        //have a user hide connection button
                                        
                                        
                                    } else {
                                        
                                    }
                                    
                                    self.recipient.addObject(user.objectId!)
                                })
                                
                            } else {
                                let alertController = UIAlertController(title: "Sorry! You have to wait till your baby connects", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                    
                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                                    self.presentViewController(vc, animated: true, completion: nil)
                                    
                                })
                                alertController.addAction(defaultAction)
                                self.presentViewController(alertController, animated: true, completion: nil)
                            }
                        } else {
                            print(error?.localizedDescription)
                        }
                    })
                    
                    
                    
                    
                    /*
                     let alertController = UIAlertController(title: "Sorry! You have to wait till your baby connects", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                     
                     let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                     
                     let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                     let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                     self.presentViewController(vc, animated: true, completion: nil)
                     
                     })
                     alertController.addAction(defaultAction)
                     self.presentViewController(alertController, animated: true, completion: nil)
                     */
                }
                
            } else {
                print(error?.localizedDescription)
            }
        }

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        /*
        
        let sQuery:PFQuery = PFQuery(className: "SpouseReply")
        //sQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        //sQuery.whereKey("recUser", containsString: PFUser.currentUser()?.username)
        sQuery.orderByAscending("createdAt")
        sQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if objects != nil {
                
                for otherObject in objects!{
                    
                    let otherName:String! = otherObject.objectForKey("other") as! String
                    
                    if PFUser.currentUser()?.username == otherName {
                        self.mySpouseArray = objects!
                        print(self.mySpouseArray.count)
                        
                        if self.mySpouseArray.count >= 1 {
                            
                            self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                            let squery = self.spouseRelation.query()
                            squery.orderByAscending("username")
                            squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                                
                                if error != nil{
                                    print(error?.localizedDescription)
                                } else {
                                    self.spouse = objects!
                                    
                                    if error != nil{
                                        print(error?.localizedDescription)
                                    } else {
                                        self.spouse = objects!
                                        
                                        self.image = UIImage()
                                        self.imagePicker.delegate = self
                                        self.imagePicker.allowsEditing = false
                                        //imagePicker.sourceType = .Camera
                                        self.imagePicker.videoMaximumDuration = 6.0
                                        
                                        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                                            
                                            self.imagePicker.sourceType = .Camera
                                            
                                        } else {
                                            
                                            self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                                        }
                                        self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(self.imagePicker.sourceType)!
                                        
                                        self.presentViewController(self.imagePicker, animated: false, completion: nil)
                                        
                                    }
                                    
                                    
                                }
                                
                                let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                                
                                if user.objectId != nil{
                                    //have a user hide connection button
                                    
                                    
                                } else {
                                    
                                }
                                
                                //self.recipient.addObject(user.objectId!)
                            })
                            
                            
                        }
                    }
                    
                }
                
                
                for object in objects! {
                    
                    let name:String! = object.objectForKey("currentUser") as! String
                    if PFUser.currentUser()?.username == name{
                        
                        
                        self.mySpouseArray = objects!
                        print(self.mySpouseArray.count)
                        print(self.mySpouseArray)
                        
                        if self.mySpouseArray.count >= 1 {
                            //cool to enter initialize spouseRelation
                            self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                            let squery = self.spouseRelation.query()
                            squery.orderByAscending("username")
                            squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                                
                                if error != nil{
                                    print(error?.localizedDescription)
                                } else {
                                    self.spouse = objects!
                                    
                                    self.image = UIImage()
                                    self.imagePicker.delegate = self
                                    self.imagePicker.allowsEditing = false
                                    //imagePicker.sourceType = .Camera
                                    self.imagePicker.videoMaximumDuration = 6.0
                                    
                                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                                        
                                        self.imagePicker.sourceType = .Camera
                                        
                                    } else {
                                        
                                        self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                                    }
                                    self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(self.imagePicker.sourceType)!
                                    
                                    self.presentViewController(self.imagePicker, animated: false, completion: nil)
                                }
                                
                            })
                            
                        } else {
                            //check if you have an accepted connection
                            
                            let aQuery:PFQuery = PFQuery(className: "AcceptedConnection")
                            aQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
                            aQuery.orderByAscending("createdAt")
                            aQuery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
                                
                                if objects != nil {
                                    self.acceptedArray = objects!
                                    if self.acceptedArray.count == 1{
                                        //yay they can do their thang
                                        
                                        self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                                        let squery = self.spouseRelation.query()
                                        squery.orderByAscending("username")
                                        squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                                            
                                            if error != nil{
                                                print(error?.localizedDescription)
                                            } else {
                                                self.spouse = objects!
                                                
                                                self.image = UIImage()
                                                self.imagePicker.delegate = self
                                                self.imagePicker.allowsEditing = false
                                                //imagePicker.sourceType = .Camera
                                                self.imagePicker.videoMaximumDuration = 6.0
                                                
                                                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                                                    
                                                    self.imagePicker.sourceType = .Camera
                                                    
                                                } else {
                                                    
                                                    self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                                                }
                                                self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(self.imagePicker.sourceType)!
                                                
                                                self.presentViewController(self.imagePicker, animated: false, completion: nil)
                                                
                                            }
                                            
                                            
                                            //self.recipient.addObject(user.objectId!)
                                        })
                                        
                                    } else {
                                        let alertController = UIAlertController(title: "Sorry! You have to wait till your baby connects", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                        
                                        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                            
                                            let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                            let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                                            self.presentViewController(vc, animated: true, completion: nil)
                                            
                                        })
                                        alertController.addAction(defaultAction)
                                        self.presentViewController(alertController, animated: true, completion: nil)
                                    }
                                } else {
                                    print(error?.localizedDescription)
                                }
                            })
                            
 
                            
                        }}}
 
                
                
            } else {
                
                //no match
                
                
                
                
                
            }
            
        }
        
       */
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let query:PFQuery = PFQuery(className:"Messages")
        query.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        query.orderByDescending("createdAt")
        query.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                //we have a message
                self.messages = objects!
                print(self.messages.count)
                
                self.tableView.reloadData()
            }
        }
        
        /*
         let user:PFUser = allUsers.objectAtIndex(indexPath.row) as! PFUser
         let spouseRelation:PFRelation = self.currentUser.relationForKey("spouseRelation")
         spouseRelation.addObject(user)
         
         currentUser.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
         
         if success {
         print("successful relation")
         }
         }
         */
        
        let squery = spouseRelation.query()
        squery.orderByAscending("username")
        squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.allUsers = objects!
                //print(self.allUsers)
                self.tableView.reloadData()
            }
            
            let user:PFUser = self.allUsers.objectAtIndex(0) as! PFUser
            print(user)
        })
        
        
        
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        if mediaType.isEqualToString(kUTTypeImage as NSString as String){
            
            image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            if imagePicker.sourceType == .Camera{
                
                UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
                //self.dismissViewControllerAnimated(true, completion: nil)
            }
            
            
        } else if mediaType.isEqualToString(kUTTypeMovie as NSString as String){
            
            videoFilePath = info[UIImagePickerControllerMediaURL]!.path
            if imagePicker.sourceType == .Camera{
                if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(videoFilePath){
                    
                    UISaveVideoAtPathToSavedPhotosAlbum(videoFilePath, nil, nil, nil)
                    // self.dismissViewControllerAnimated(true, completion: nil)
                }
            }
            
        }
        
        picker.dismissViewControllerAnimated(false, completion: nil)
        self.tableView.reloadData()
        //presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
        //dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allUsers.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        
        // Configure the cell...
        //let user:PFUser = allUsers.objectAtIndex(indexPath.row) as! PFUser
        //cell.textLabel?.text = user.username
        cell.textLabel?.text = "Tap me! ReIgnite your relationship!"
        cell.textLabel?.textColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
        return cell
    }
    
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        
        //let cell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        user = allUsers.objectAtIndex(indexPath.row) as! PFUser
        print(user)
        if counter < 1{
            /*
             if cell.accessoryType == .None{
             cell.accessoryType = .Checkmark
             //recipients.addObject(user.objectId!)
             } else {
             
             }
             */
            if let myImage = image{
                print(myImage)
                
                uploadMessage()
                
            } else {
                //alert no image
                let alertController = UIAlertController(title: "no user selected", message: "Please check your baby", preferredStyle: UIAlertControllerStyle.Alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    
                })
                alertController.addAction(defaultAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            
        } else{
            recipients.removeAllObjects()
        }
        
        
        
    }
    
    
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        
        
        reset()
        //send home
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
        
        
    }
    
    @IBAction func sendButtonTapped(sender: AnyObject) {
        //let myImage = image
        print("sendButton=\(user)")
        
        if user == ""{
            print("no user")
        } else {
            print(" user")
        }
        
        
        if let myImage = image{
            print(myImage)
            
            uploadMessage()
            
        } else {
            //alert no image
            let alertController = UIAlertController(title: "no user selected", message: "Please check your baby", preferredStyle: UIAlertControllerStyle.Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
            })
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        
        
        
        
        
    }
    
    func uploadMessage() {
        
        let message = PFObject(className: "Messages")
        if videoFilePath == nil{
            //determine what device the phone is running one
            let newImage = resizeImage(image!, width: 750.0, height: 1334.0)
            
            let imageData = UIImagePNGRepresentation(newImage)
            let imageFile = PFFile(name: "image.png", data: imageData!)
            
            
            message["imageName"] = "Sexy Image"
            message["imageFile"] = imageFile
            message["fileType"] = "image"
        }
        else {
            print("YAY")
            let fileData = NSData(contentsOfFile: videoFilePath)
            let imageFile = PFFile(name: "video.mov", data: fileData!)
            message["fileType"] = "video"
            message["imageFile"] = imageFile
            
            
        }
        let user:PFUser = self.allUsers.objectAtIndex(0) as! PFUser
        recipients.addObject(user.objectId!)
        message.setObject(recipients, forKey: "recipientsIds")
        message.setObject(PFUser.currentUser()!, forKey: "senderId")
        message.setObject((PFUser.currentUser()?.username)!, forKey: "senderName")
        message.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
            if success{
                print("YAY")
                //alert that it was successful
                let alertController = UIAlertController(title: "Message sent successfully", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                   
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                    self.presentViewController(vc, animated: true, completion: nil)
                    self.reset()
                    
                })
                alertController.addAction(defaultAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        })
        
        
    }
    
    func reset() {
        image = nil
        recipients.removeAllObjects()
        videoFilePath = nil
    }
    
    func resizeImage(image:UIImage, width:CGFloat, height:CGFloat)->UIImage{
        
        let newSize = CGSizeMake(width, height)
        let newRectangle = CGRectMake(0, 0, width, height)
        UIGraphicsBeginImageContext(newSize)
        image.drawInRect(newRectangle)
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resizedImage
        
    }
    
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        self.dismissViewControllerAnimated(false, completion: nil)
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
}
