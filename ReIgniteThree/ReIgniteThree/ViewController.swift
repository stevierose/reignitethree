//
//  ViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/6/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

class ViewController: UIViewController {
    
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        signInButton.layer.cornerRadius = 5
        signInButton.layer.borderWidth = 1
        signInButton.layer.borderColor = UIColor.blackColor().CGColor
        
        registerButton.layer.cornerRadius = 5
        registerButton.layer.borderWidth = 1
        registerButton.layer.borderColor = UIColor.blackColor().CGColor
       
        self.hideKeyboardWhenTappedAround()
        
        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
        }
        
        // Do any additional setup after loading the view, typically from a nib.
        /*
        let dateRequest = PFObject(className: "DateRequest")
        dateRequest["location"] = "Farenheit"
        dateRequest.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            if error != nil {
                
            }
            else {
                print("Date Request saved")
            }
        }
        */
        /*
        let dateRequestQuery = PFQuery(className: "DateRequest")
        //dateRequestQuery.whereKey("location", equalTo: "Farenheit")
        dateRequestQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            if error != nil{
                
            } else {
                for object in objects!{
                    print(object.objectId)
                }
                
                
            }
        }
        
        */
    }
  

    @IBAction func signInButtonTapped(sender: AnyObject) {
        
        let username = usernameField.text
        let password = passwordField.text
        
        PFUser.logInWithUsernameInBackground(username!, password:password!) {
            (user: PFUser?, error: NSError?) -> Void in
            if user != nil {
                // Do stuff after successful login.
                
                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                self.presentViewController(vc, animated: true, completion: nil)

            } else {
                // The login failed. Check error to see why.
                let alertController = UIAlertController(title: "Username and/or password is incorrect", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    self.usernameField.text = ""
                    self.passwordField.text = ""
                })
                alertController.addAction(defaultAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }

    }
    @IBAction func registerButtonTapped(sender: AnyObject) {
        
        
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("RegisterViewController") as! RegisterViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
        

    }
    @IBAction func forgotMyPasswordButtonTapped(sender: AnyObject) {
        /*
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("DateRequestViewController") as! DateRequestViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
        */
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ResetPasswordViewController") as! ResetPasswordViewController
        self.presentViewController(vc, animated: true, completion: nil)
        
    }
    
   
}

