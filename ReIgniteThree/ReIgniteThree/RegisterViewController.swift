//
//  RegisterViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/6/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class RegisterViewController: UIViewController {

    @IBOutlet var usernameField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var confirmPasswordField: UITextField!
    @IBOutlet var registerButton: UIButton!
    var username:String!
    var email:String!
    var password:String!
    var confirmPassword:String!
    var isEmail:Bool! = false
    var isPassword:Bool! = false
    var isUsername:Bool! = false
    var isConfirmPassword:Bool! = false
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        if let nav = navigationController?.navigationBar{
           
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.grayColor()]
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registerButtonTapped(sender: AnyObject) {
        username = usernameField.text
        email = emailField.text
        password = passwordField.text
        confirmPassword = confirmPasswordField.text
        
        let user = PFUser()
        if username.characters.count >= 3{
            isUsername = true
            user.username = username
            
        } else {
            //alert invalid email
            //clear username text and password text
            isUsername = false
            let alertController = UIAlertController(title: "Username needs to have at least four characters", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
                self.clearText()
            })
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            

            
        }
        
        if validate(email) && emailField.text?.characters.count >= 2{
            isEmail = true
            user.email = email
            
        } else {
            isEmail = false
            // alert invalid email
            //clear text inputs
            let alertController = UIAlertController(title: "Your email is not valid", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
                self.clearText()
            })
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
           
        }

        
        if password.characters.count > 3 {
            isPassword = true
            
          print("password has enough characters")
            
        } else {
            isPassword = false
            //alert invalid password
            //clear username text and password text
            let alertController = UIAlertController(title: "Your password needs to have at least four characters", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
                self.clearText()
            })
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }
        
        if password == confirmPassword {
            isConfirmPassword = true
              user.password = password
            
            
            
        } else {
            self.isConfirmPassword = false
            
            let alertController = UIAlertController(title: "Your password does not match", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
                self.clearText()
            })
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }
        
        if isConfirmPassword == true && isPassword == true && isEmail == true && isUsername == true{
            user.signUpInBackgroundWithBlock {
                (succeeded: Bool, error: NSError?) -> Void in
                if let error = error {
                    let errorString = error.userInfo["error"] as? NSString
                    
                    print(errorString)
                    
                } else {
                    
                    
                    let controller = self.storyboard!.instantiateViewControllerWithIdentifier("SearchViewController") as! SearchViewController
                    
                    let controllerNav = UINavigationController(rootViewController: controller)
                    
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.window?.rootViewController = controllerNav
                    
                }
            }

        } else {
            print("all data doesnt match")
        }
       
    }

    @IBAction func cancelButtonTapped(sender: AnyObject) {
        
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav

    }
    
    func isValidPassword(candidate:String!)->Bool{
        
        let passwordRegex = "(?=.*[a-z])(?=.*[A-Z](?=.*\\d).{6,10}"
        
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluateWithObject(candidate)
    }
    
    func validate(YourEMailAddress: String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluateWithObject(YourEMailAddress)
    }
    
    func registerAlert() {
        
        let alertController = UIAlertController(title: "Username and/or password fields not completed", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
        })
        alertController.addAction(defaultAction)
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    func clearText() {
        usernameField.text = ""
        emailField.text = ""
        passwordField.text = ""
        confirmPasswordField.text = ""
    }
   
}
