//
//  AcceptedDateRequestViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/15/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class AcceptedDateRequestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate {
    
    var spouseRelation:PFRelation = PFRelation()
    var recipient:NSMutableArray = []
    var spouse:NSArray = []
    var date:NSArray = []
    var user:PFUser = PFUser()
    var theUser:PFUser = PFUser()
    var status:String!
    var comment:String!
    var ddate:String!
    var location:String!
    var time:String!
    var counter = 0
    var confirmedDate:String!
    var confirmedDressType:String!
    var confirmedLocation:String!
    var mySpouseArray:NSArray = []
    var acceptedArray:NSArray = []
    
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        if let nav = navigationController?.navigationBar{
          
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
        }
        
        let fQuery:PFQuery = PFQuery(className: "SpouseReply")
        //fQuery.whereKey("other", containsString: PFUser.currentUser()?.username)
        fQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) in
            
            if objects != nil {
                
            }
            
        }
        
        
        
        
        print("name doesnt match")
        
        
        let sQuery:PFQuery = PFQuery(className: "SpouseReply")
        sQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        //sQuery.whereKey("recUser", containsString: PFUser.currentUser()?.username)
        sQuery.orderByAscending("createdAt")
        sQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if objects != nil {
                
                
                self.mySpouseArray = objects!
                print(self.mySpouseArray.count)
                print(self.mySpouseArray)
                
                
                
                if self.mySpouseArray.count == 1 {
                    //cool to enter initialize spouseRelation
                    self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                    let squery = self.spouseRelation.query()
                    squery.orderByAscending("username")
                    squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                        
                        if error != nil{
                            print(error?.localizedDescription)
                        } else {
                            self.spouse = objects!
                            
                        }
                        
                        let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                        
                        if user.objectId != nil{
                            //have a user hide connection button
                            
                            
                        } else {
                            
                        }
                        
                        self.recipient.addObject(user.objectId!)
                    })
                    
                } else {
                    //check if you have an accepted connection
                    
                    let aQuery:PFQuery = PFQuery(className: "AcceptedConnection")
                    aQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
                    aQuery.orderByAscending("createdAt")
                    aQuery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
                        
                        if objects != nil {
                            self.acceptedArray = objects!
                            if self.acceptedArray.count >= 1{
                                //yay they can do their thang
                                
                                self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                                let squery = self.spouseRelation.query()
                                squery.orderByAscending("username")
                                squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                                    
                                    if error != nil{
                                        print(error?.localizedDescription)
                                    } else {
                                        self.spouse = objects!
                                        
                                    }
                                    
                                    let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                                    
                                    if user.objectId != nil{
                                        //have a user hide connection button
                                        
                                        
                                    } else {
                                        
                                    }
                                    
                                    self.recipient.addObject(user.objectId!)
                                })
                                
                            } else {
                                let alertController = UIAlertController(title: "Sorry! You have to wait till your baby connects", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                    
                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                                    self.presentViewController(vc, animated: true, completion: nil)
                                    
                                })
                                alertController.addAction(defaultAction)
                                self.presentViewController(alertController, animated: true, completion: nil)
                            }
                        } else {
                            print(error?.localizedDescription)
                        }
                    })
                    
                    
                    
                    
                    /*
                     let alertController = UIAlertController(title: "Sorry! You have to wait till your baby connects", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                     
                     let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                     
                     let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                     let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                     self.presentViewController(vc, animated: true, completion: nil)
                     
                     })
                     alertController.addAction(defaultAction)
                     self.presentViewController(alertController, animated: true, completion: nil)
                     */
                }
                
            } else {
                print(error?.localizedDescription)
            }
        }

        
        
        
        
        
        
        
       
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let query = spouseRelation.query()
        query.orderByAscending("username")
        query.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.spouse = objects!
                
            }
            
            self.user = self.spouse.objectAtIndex(0) as! PFUser
            self.theUser = self.user
            print(self.user.objectId)
            self.recipient.addObject(self.user.objectId!)
        })
        
      
       print("myuser\(theUser.objectId)")
        
        let dateQuery:PFQuery = PFQuery(className:"AcceptedDateRequest")
        dateQuery.whereKey("status", containsString: "accepted")
        dateQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        //dateQuery.whereKey("senderName", equalTo: (PFUser.currentUser()?.username)!)
        dateQuery.orderByDescending("createdAt")
        dateQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                //we have a message
                self.date = objects!
                print(self.date.count)
                self.tableView.reloadData()
                //let dates:PFObject = self.date.objectAtIndex(0) as! PFObject
            }
        }
            //Accepted request for my baby
        /*
        let sdateQuery:PFQuery = PFQuery(className:"DateRequest")
        sdateQuery.whereKey("status", containsString: "accepted")
        //sdateQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        sdateQuery.whereKey("senderName", equalTo: (PFUser.currentUser()?.username)!)
        sdateQuery.orderByDescending("createdAt")
        sdateQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                //we have a message
                self.date = objects!
                print(self.date.count)
                self.tableView.reloadData()
                //let dates:PFObject = self.date.objectAtIndex(0) as! PFObject
            }
        }

        */

    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:AcceptedDateTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! AcceptedDateTableViewCell
        
        
        let dates:PFObject = date.objectAtIndex(indexPath.row) as! PFObject
        //print(cell.status.text?.characters.count)
        if cell.dress.text?.characters.count < 1{
            print("no status must be other user")
        } else {
            print("status")
            
            
            ddate = dates.objectForKey("datePicker") as? String
            location = dates.objectForKey("location") as? String
            //status = dates.objectForKey("status") as? String
            //time = dates.objectForKey("time") as? String
            comment = dates.objectForKey("dateComments") as? String
            print(comment)
            confirmedDate = dates.objectForKey("confirmedDate") as? String
            confirmedLocation = dates.objectForKey("confirmedLocation") as? String
            confirmedDressType = dates.objectForKey("confirmedDressType") as? String
            //print(confirmedDressType)
            //print(confirmedLocation)
            //print(confirmedDate)
            
            
            if let theDate = confirmedDate{
                
                let myDate = theDate
                if myDate == ""{
                    
                    print("no date")
                    ddate = ""
                }else {
                    
                    cell.date.text = myDate
                }
                
            } else {
                print("no date")
            }
       
            if let theLocation = confirmedLocation{
                
                let myLocation = theLocation
                
                if myLocation == ""{
                    print("no location")
                    location = ""
                } else {
                    cell.location.text = myLocation
                }
            } else {
                print("no locale")
            }
            
            if let theDressStatus = confirmedDressType{
                
                let myDressStatus = theDressStatus
               // cell.status.text = myStatus
                if myDressStatus == ""{
                    
                    print("no status")
                    status = ""
                } else {
                    cell.dress.text = myDressStatus
                }
            } else {
                print("no stat-us")
            }
           
            if let theComment = comment{
                
                let myComment = theComment
                if myComment == ""{
                    
                    print("no comment listed")
                    comment = ""
                } else {
                    
                    cell.comments.text = myComment
                }
            } else {
                print("no comment please")
            }
         
           
            /*
            cell.status.text = status
            cell.location.text = location
            cell.date.text = ddate
            //cell.time.text = time
            cell.comments.text = comment
            */

        }
            //print(dates.objectForKey("date") as? String)
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        print("user tapped on row: \(indexPath.row)")
        
        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? AcceptedDateTableViewCell {
            let label = cell.comments
            tableView.beginUpdates()
            label.numberOfLines = label.numberOfLines == 0 ? 1 : 0
            tableView.endUpdates()
        }

        
        
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return date.count
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("DateHistoryViewController") as! DateHistoryViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav

        
    }
    
}
