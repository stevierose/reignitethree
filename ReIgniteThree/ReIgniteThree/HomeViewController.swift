//
//  HomeViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/6/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
import MediaPlayer
import AVKit
import AVFoundation

// UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource
class HomeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource{
    @IBOutlet var menuButton: UIBarButtonItem!
    @IBOutlet var dateContainer: UIView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var locationLabel: UIView!
    @IBOutlet var dressTypeLabel: UILabel!
    @IBOutlet var commentField: UITextView!
    @IBOutlet var connectonButton: UIButton!
    @IBOutlet var connectButton: UIBarButtonItem!
   
    
  
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var sendDateRequestButton: UIButton!
    @IBOutlet var sexyMessage: UIButton!
    @IBOutlet var reIgniteIdeasButton: UIButton!
    let imagePicker:UIImagePickerController = UIImagePickerController()
    var videoFilePath:String!
    var spouseRelation:PFRelation!
    var messages:NSArray = []
    var date:NSArray = []
    var selectedMessage:PFObject!
    var moviePlayer:AVPlayerViewController!
    var spousesReply:NSArray = []
    var acceptedConnections:NSArray = []
    var declinedConnections:NSArray = []
    var allUsers:NSArray = []
    var image:UIImage = UIImage()
    var session:NSTimer!
    var secondSession:NSTimer!
    var thirdSession:NSTimer!
    var fourthSession:NSTimer!
    var isConnected:Bool!
    var counter:Int!
    var spouse:NSArray = []
    var onlyUsers:NSArray = []
    var onlyUser:PFObject!
    var currentUser:PFUser!
    var spouseObjects:NSArray = []

    var tableData:[String] = ["Send a date request", "Send a sexy thought", "News feed", "Date History", "Messages", "ReIgnite Events"]
    var tableImages:[String] = ["dateRequest.png", "heart.png", "newsFeed.png", "fireRose.png", "messageInbox.png", "events.png"]
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        connectionReplyTimer()
        self.connectButton.enabled = false
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

       
        
        if let nav = navigationController?.navigationBar{
       
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.grayColor()]
            
            
        }
       
        let notificationSettings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
        
       

        moviePlayer = AVPlayerViewController()
        tableView.delegate = self
       
    
       
               
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        
       
        
        let mySpouseQuery = PFQuery(className: "SpouseReply")
        mySpouseQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        mySpouseQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            if error != nil {
                
            } else {
                self.spouseObjects = objects!
                
                print(self.spouseObjects)
                for spouseObject in self.spouseObjects{
                    print(spouseObject.objectId)
                    
                    self.spouseRelation = PFRelation()
                    self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                    let sQuery = self.spouseRelation.query()
                    sQuery.orderByAscending("username")
                    sQuery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                        
                        if error != nil{
                            print(error?.localizedDescription)
                        } else {
                            self.spouse = objects!
                            
                            let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                            
                            if user.objectId != nil{
                                //have a user hide connection button
                                //self.connectonButton.hidden = true
                                self.stopTimer()
                                //self.connectButton.enabled = false
                                //self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
                                
                                
                                
                            } else {
                                //start timers
                                self.connectionReplyTimer()
                                //self.replyRequestTimer()
                                //self.rejectRequestTimer()
                            }
                            
                            
                        }
                        
                    })

                }
            }
        }

        
        
        /*
        if let mySpouseRelation = spouseRelation{
            
            var theSpouseRelation = mySpouseRelation
            
            theSpouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
            
            let sQuery = theSpouseRelation.query()
            sQuery.orderByAscending("username")
            sQuery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                
                if error != nil{
                    print(error?.localizedDescription)
                } else {
                    self.spouse = objects!
                    
                }
                
                let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                
                if user.objectId != nil{
                    //have a user hide connection button
                    //self.connectonButton.hidden = true
                    self.connectButton.enabled = false
                    self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
                    
                    
                    
                } else {
                    //start timers
                    self.connectionReplyTimer()
                    self.replyRequestTimer()
                    self.rejectRequestTimer()
                }
                
                //self.recipient.addObject(user.objectId!)
            })

  
        } else {
            print("no relation yet")
        }
        */
        
        let query:PFQuery = PFQuery(className:"Messages")
        query.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        query.orderByDescending("createdAt")
        query.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                //we have a message
                self.messages = objects!
                print(self.messages.count)
                
                self.tableView.reloadData()
            }
        }
        let me = PFUser.currentUser()?.objectId
        print(me)
        let dateQuery:PFQuery = PFQuery(className:"DateRequest")
        dateQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        dateQuery.orderByDescending("createdAt")
        dateQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                //we have a message
                self.date = objects!
                print(self.date.count)
                self.tableView.reloadData()
                //let dates:PFObject = self.date.objectAtIndex(0) as! PFObject
            }
        }

       
        
        
    }



    @IBAction func sendDateRequestButtonTapped(sender: AnyObject) {
        /*
        //showDateRequestor()
        sendDateRequestButton.enabled = false
        sendDateRequestButton.backgroundColor = UIColor.grayColor()
        sexyMessage.enabled = false
        sexyMessage.backgroundColor = UIColor.grayColor()
        reIgniteIdeasButton.enabled = false
        reIgniteIdeasButton.backgroundColor = UIColor.grayColor()
        */
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("DateRequestViewController") as! DateRequestViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
        
    
    }
    @IBAction func sexyMessageButtonTapped(sender: AnyObject) {
        
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("VideoTableViewController") as! VideoTableViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
    }


    @IBAction func requestSentButtonTapped(sender: AnyObject) {
        //check for errors
        //hide requestor
        //save to parse
        //alert request has been sent
        //go to date history view
       
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
      
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        /*
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        
        // Configure the cell...
        let message:PFObject = messages.objectAtIndex(indexPath.row) as! PFObject
        cell.textLabel?.text = message.objectForKey("senderName") as? String
        
        let file:NSString! = message.objectForKey("fileType") as! NSString
        if file.isEqualToString("image"){
            cell.imageView?.image = UIImage(named: "icon_image")
        } else {
            cell.imageView?.image = UIImage(named: "icon_video")
        }
        */

        
        let cell:CustomTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! CustomTableViewCell
        /*
         print(self.date.count)
        let dates:PFObject = date.objectAtIndex(indexPath.row) as! PFObject
        cell.timeLabel?.text =  dates.objectForKey("time") as? String
        cell.dateLabel?.text = dates.objectForKey("date") as? String
        */
        cell.imageView?.image = UIImage(named: tableImages[indexPath.row])
        cell.tableNames.text = tableData[indexPath.row]
        
        
     
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        //selectedMessage = tableData.objectAtIndex(indexPath.row) as! PFObject
     
       

         tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if indexPath.row == 0{
            //send a date request
           
            
            let controller = self.storyboard!.instantiateViewControllerWithIdentifier("DateRequestViewController") as! DateRequestViewController

            
            let controllerNav = UINavigationController(rootViewController: controller)

            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            appDelegate.window?.rootViewController = controllerNav
                
                
            
            
        } else if indexPath.row == 1{
            //send a sexy thought
            let controller = self.storyboard!.instantiateViewControllerWithIdentifier("VideoTableViewController") as! VideoTableViewController
            
            let controllerNav = UINavigationController(rootViewController: controller)
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window?.rootViewController = controllerNav
            
        } else if indexPath.row == 2{
            //news feed
            let controller = self.storyboard!.instantiateViewControllerWithIdentifier("NewsFeedViewController") as! NewsFeedViewController
            
            let controllerNav = UINavigationController(rootViewController: controller)
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window?.rootViewController = controllerNav
            
        } else if indexPath.row == 3{
            //Date history
            let controller = self.storyboard!.instantiateViewControllerWithIdentifier("DateHistoryViewController") as! DateHistoryViewController
            
            let controllerNav = UINavigationController(rootViewController: controller)
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window?.rootViewController = controllerNav
            
            
        } else if indexPath.row == 4{
            //messages
            let controller = self.storyboard!.instantiateViewControllerWithIdentifier("MessageInboxViewController") as! MessageInboxViewController
            
            let controllerNav = UINavigationController(rootViewController: controller)
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window?.rootViewController = controllerNav
            
            
        } else if indexPath.row == 5{
            //take me out  TakeMeOutViewController
            let controller = self.storyboard!.instantiateViewControllerWithIdentifier("TakeMeOutViewController") as! TakeMeOutViewController
            
            let controllerNav = UINavigationController(rootViewController: controller)
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window?.rootViewController = controllerNav
            
        }
        
        /*
        let sQuery = spouseRelation.query()
        sQuery.orderByAscending("username")
        sQuery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.spouse = objects!
                
            }
            
            let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
            
            if user.objectId != nil{
                //have a user hide connection button
               // self.connectonButton.hidden = true
                print("YAY it's cool")
                
                
            } else {
                //start timers
                
                self.stopTimer()
            }
            
            //self.recipient.addObject(user.objectId!)
        })
        */
    }
    
    
    
    func disposeVideo(){
        
        var recipientIds:NSMutableArray = []
        recipientIds = selectedMessage.objectForKey("recipientsIds") as! NSMutableArray
        print(recipientIds)
        
        selectedMessage.deleteInBackground()
        recipientIds.removeAllObjects()
        selectedMessage.setObject(recipientIds, forKey: "recipientsIds")
        selectedMessage.saveInBackground()
        navigationController?.popToRootViewControllerAnimated(true)

    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showImage"{
            
            var ivc = ImageViewController()
            ivc = segue.destinationViewController as! ImageViewController
            ivc.message = selectedMessage
            
        }
    }
 
    @IBAction func acceptButtonTapped(sender: AnyObject) {
    }
    
    @IBAction func declineButtonTapped(sender: AnyObject) {
    }
    
    func connectionReplyTimer(){
        
    }
    
    func replyRequestTimer() {
        
        secondSession = NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: #selector(HomeViewController.replyRequest), userInfo: nil, repeats: false)
    }
    
    
    func rejectRequestTimer() {
         thirdSession = NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: #selector(HomeViewController.rejectRequest), userInfo: nil, repeats: false)
    }
    
    func destroyVideo(){
        fourthSession = NSTimer.scheduledTimerWithTimeInterval(6.0, target: self, selector: #selector(HomeViewController.disposeVideo), userInfo: nil, repeats: false)
    }
    
    func stopTimer() {
        session.invalidate()
        
        //secondSession.invalidate()
        //thirdSession.invalidate()
        //fourthSession.invalidate()
        
    }
    
    func replyRequest() {
        
        let acceptedQuery:PFQuery = PFQuery(className:"AcceptedConnection")
        
        acceptedQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        acceptedQuery.orderByDescending("createdAt")
        acceptedQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                
                self.acceptedConnections = objects!
                print(self.acceptedConnections.count)
                if self.acceptedConnections.count == 1{
                    
                    let connections:PFObject = self.acceptedConnections.objectAtIndex(0) as! PFObject
                    
                    let notification = UILocalNotification()
                    notification.fireDate = NSDate(timeIntervalSinceNow: 5)
                    notification.alertBody = "Your request has been accepted from your baby"
                    notification.alertAction = connections.objectForKey("accept") as? String
                    notification.soundName = UILocalNotificationDefaultSoundName
                    //notification.userInfo = ["CustomField1": "w01t"]
                    UIApplication.sharedApplication().scheduleLocalNotification(notification)
                    
                } else {
                    print("no accepted request")
                }
      
            }
            //self.session.invalidate()
        }
        
      
        
    }
    
    func rejectRequest() {
        
        let declinedQuery:PFQuery = PFQuery(className:"DeclinedConnection")
        
        declinedQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        declinedQuery.orderByDescending("createdAt")
        declinedQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                
                self.declinedConnections = objects!
                print(self.declinedConnections.count)
                if self.declinedConnections.count == 1{
                    
                    let notification = UILocalNotification()
                    notification.fireDate = NSDate(timeIntervalSinceNow: 5)
                    notification.alertBody = "Sorry! Your request was denied"
                    notification.alertAction = "Check it out!"
                    notification.soundName = UILocalNotificationDefaultSoundName
                    notification.userInfo = ["CustomField1": "w00t"]
                    UIApplication.sharedApplication().scheduleLocalNotification(notification)
                    
                    //self.navigationController?.popViewControllerAnimated(true)
                    
                    self.selectedMessage = self.messages.objectAtIndex(0) as! PFObject
                    
                } else {
                    print("no decline request")
                }
                
            }
            
        }

    }
    
    
    
    func spouseReply(){
      
        
        
        let spouseQuery:PFQuery = PFQuery(className:"SpouseReply")
      
        spouseQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        spouseQuery.orderByDescending("createdAt")
        spouseQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                
                self.spousesReply = objects!
                print(self.spousesReply.count)
                if self.spousesReply.count >= 1{
                    
                    self.connectButton.enabled = true
                    self.navigationItem.rightBarButtonItem?.tintColor = UIColor.grayColor()
                    
                    let notification = UILocalNotification()
                    notification.fireDate = NSDate(timeIntervalSinceNow: 5)
                    notification.alertBody = "Hey you! Yeah you! You have a connection request!"
                    notification.alertAction = "Check it out!"
                    notification.soundName = UILocalNotificationDefaultSoundName
                    notification.userInfo = ["CustomField1": "w00t"]
                    UIApplication.sharedApplication().scheduleLocalNotification(notification)
                    self.stopTimer()

                    
                                        //self.navigationController?.popViewControllerAnimated(true)
                    /*
                    let alertController = UIAlertController(title: "New Connection", message: "Check it out!", preferredStyle: UIAlertControllerStyle.Alert)
                    let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                        
                        //self.selectedMessage = self.messages.objectAtIndex(0) as! PFObject
                        
                        
                        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("ConnectionReplyViewController") as! ConnectionReplyViewController
                        
                        let controllerNav = UINavigationController(rootViewController: controller)
                        
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.window?.rootViewController = controllerNav
                        
                    })
                    
                    alertController.addAction(defaultAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                    */
                    
                } else {
                    print("no spouse request")
                }
              
            }
            
        }

    }
    
    @IBAction func connectionButtonTapped(sender: AnyObject) {
       
        
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("ConnectionReplyViewController") as! ConnectionReplyViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
        
    }
    
    @IBAction func logoutButtonTapped(sender: AnyObject) {
 
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
        
        PFUser.logOut()
    }
    
   
    
    override func viewWillDisappear(animated: Bool) {
        
        if session.valid{
            //print("timer stopped")
            stopTimer()
        } else {
            print("timer stopped")
            //stopTimer()
        }
        
    }

}
