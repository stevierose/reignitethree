//
//  Place.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/22/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import Foundation

class Place {
    
    var name = ""
    var rating:Double = 0.0
    var phoneNumber = ""
    var address = ""
    var openNow = ""
    var icon:NSData! = NSData()
    var lat:Double = 0.0;
    var lng:Double = 0.0;
    var newName:String!
    var newRating:Double!
    var newPhoneNumber:String!
    var newAddress:String!
    var newOpenNow:String!
    var newLat:Double = 0.0
    var newLng:Double = 0.0
    var newIcon:NSData!
    
   
    var personIcon:(NSData!){
        
        get{
            return icon
        } set(newVal) {
            
        }
    }
    var personName:(String!){
        
        get{
            return name
        } set(newVal) {
            
            newName = newVal
            
        }
    }
    
    var personOpenNow:(String!){
        
        get{
            return openNow
        } set (newVal){
            
            newOpenNow = newVal
        }
    }
    
    var personRating:(Double!) {
        
        get{
            return rating
        } set(newVal){
            
            newRating = newVal
        }
        
    }
    
    var personNumber:(String!) {
        get{
          return phoneNumber
            
        } set(newVal){
            newPhoneNumber = newVal
        }
    }
    
    var personAddress:(String!) {
        
        get{
            return address
            
        } set(newVal){
        
            newAddress = newVal
        }
    }
    
    var personLat:(Double!) {
        
        get{
            return lat
        } set(newVal) {
            newLat = newVal
        }
    }
    
    var personLng:(Double!) {
        
        get{
            return lng
        } set(newVal) {
            newLng = newVal
        }
    }
    
    
    
    
    
    
}