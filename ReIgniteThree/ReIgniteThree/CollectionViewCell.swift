//
//  CollectionViewCell.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/11/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet var cellImage: UIImageView!
    @IBOutlet var cellLabel: UILabel!
    
    
    
}
