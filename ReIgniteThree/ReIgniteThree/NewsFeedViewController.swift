//
//  NewsFeedViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/13/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class NewsFeedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate {

    @IBOutlet var postField: UITextField!
    @IBOutlet var tableView: UITableView!
    var postArray:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let nav = navigationController?.navigationBar{
      
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
        }
    }

    override func viewWillAppear(animated: Bool) {
        
        let query = PFQuery(className:"Post")
        query.orderByDescending("createdAt")
        query.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                
                self.postArray = objects!
                
                self.tableView.reloadData()
            }
        })
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postArray.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
         tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? newsFeedTableViewCell {
            let label = cell.post
            tableView.beginUpdates()
            label.numberOfLines = label.numberOfLines == 0 ? 1 : 0
            tableView.endUpdates()
        }

        
        
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let posts:PFObject = postArray.objectAtIndex(indexPath.row) as! PFObject
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! newsFeedTableViewCell
        cell.selectionStyle = .None
        cell.post.text = posts.objectForKey("thought") as? String
        cell.cUser.text = posts.objectForKey("myUser") as? String
        cell.post.numberOfLines = 1
        cell.date.text = posts.objectForKey("date") as? String
        //cell.cUser.text = PFUser.currentUser()?.username
        
        
        return cell
    }
    
    @IBAction func postButtonTapped(sender: AnyObject) {
        
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.FullStyle
        let convertedDate = dateFormatter.stringFromDate(date)
        print(convertedDate)
      
        
        let note = postField.text
        print(note?.characters.count)
        
        if note?.characters.count < 1{
            
            let alertController = UIAlertController(title: "No comment added", message: "Please enter", preferredStyle: UIAlertControllerStyle.Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
            })
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            
            
        } else {
            
            let post = PFObject(className: "Post")
            post["thought"] = note
            post["date"] = convertedDate
            post["myUser"] = PFUser.currentUser()?.username
            post.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
                
                if success{
                    
                    let controller = self.storyboard!.instantiateViewControllerWithIdentifier("NewsFeedViewController") as! NewsFeedViewController
                    
                    let controllerNav = UINavigationController(rootViewController: controller)
                    
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.window?.rootViewController = controllerNav
                    

                    
                    
                } else {
                    
                }
                
            }

        }
        
    
    }
    
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
        
        
    }
    
  }
