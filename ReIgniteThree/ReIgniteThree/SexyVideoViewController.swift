//
//  SexyVideoViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/28/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class SexyVideoViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    
    var allUsers:NSArray = []
    var currentUser = PFUser()
    var image:UIImage? = nil
    let imagePicker:UIImagePickerController = UIImagePickerController()
    var videoFilePath:String!
    var spouseRelation:PFRelation = PFRelation()
    let spouses:NSArray = []
    var recipients:NSMutableArray = []
    var counter = 0
    var messages:NSArray = []
    var user:PFUser = PFUser()
    var mySpouseArray:NSArray = []
    var acceptedArray:NSArray = []
    var spouse:NSArray = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sQuery:PFQuery = PFQuery(className: "SpouseReply")
        sQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        sQuery.orderByAscending("createdAt")
        sQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if objects != nil {
                
                self.mySpouseArray = objects!
                print(self.mySpouseArray.count)
                print(self.mySpouseArray)
                
                if self.mySpouseArray.count == 1 {
                    //cool to enter initialize spouseRelation
                    self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                    let squery = self.spouseRelation.query()
                    squery.orderByAscending("username")
                    squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                        
                        if error != nil{
                            print(error?.localizedDescription)
                        } else {
                            self.spouse = objects!
                            
                            self.image = UIImage()
                            self.imagePicker.delegate = self
                            self.imagePicker.allowsEditing = false
                            //imagePicker.sourceType = .Camera
                            self.imagePicker.videoMaximumDuration = 6.0
                            
                            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                                
                                self.imagePicker.sourceType = .Camera
                                
                            } else {
                                
                                self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                            }
                            self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(self.imagePicker.sourceType)!
                            
                            self.presentViewController(self.imagePicker, animated: false, completion: nil)
                        }
                        
                    })
                    
                } else {
                    //check if you have an accepted connection
                    
                    let aQuery:PFQuery = PFQuery(className: "AcceptedConnection")
                    aQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
                    aQuery.orderByAscending("createdAt")
                    aQuery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
                        
                        if objects != nil {
                            self.acceptedArray = objects!
                            if self.acceptedArray.count == 1{
                                //yay they can do their thang
                                
                                self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                                let squery = self.spouseRelation.query()
                                squery.orderByAscending("username")
                                squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                                    
                                    if error != nil{
                                        print(error?.localizedDescription)
                                    } else {
                                        self.spouse = objects!
                                        
                                        self.image = UIImage()
                                        self.imagePicker.delegate = self
                                        self.imagePicker.allowsEditing = false
                                        //imagePicker.sourceType = .Camera
                                        self.imagePicker.videoMaximumDuration = 6.0
                                        
                                        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
                                            
                                            self.imagePicker.sourceType = .Camera
                                            
                                        } else {
                                            
                                            self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                                        }
                                        self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(self.imagePicker.sourceType)!
                                        
                                        self.presentViewController(self.imagePicker, animated: false, completion: nil)
                                        
                                    }
                                    
                                    
                                    //self.recipient.addObject(user.objectId!)
                                })
                                
                            } else {
                                let alertController = UIAlertController(title: "Sorry! You have to wait till your baby connects", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                    
                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                                    self.presentViewController(vc, animated: true, completion: nil)
                                    
                                })
                                alertController.addAction(defaultAction)
                                self.presentViewController(alertController, animated: true, completion: nil)
                            }
                        } else {
                            print(error?.localizedDescription)
                        }
                    })
                    
                    
                    
                }}}
        
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let query:PFQuery = PFQuery(className:"Messages")
        query.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        query.orderByDescending("createdAt")
        query.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                //we have a message
                self.messages = objects!
                print(self.messages.count)
                
                
            }
        }
        
        /*
         let user:PFUser = allUsers.objectAtIndex(indexPath.row) as! PFUser
         let spouseRelation:PFRelation = self.currentUser.relationForKey("spouseRelation")
         spouseRelation.addObject(user)
         
         currentUser.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
         
         if success {
         print("successful relation")
         }
         }
         */
        
        let squery = spouseRelation.query()
        squery.orderByAscending("username")
        squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.allUsers = objects!
                //print(self.allUsers)
                
            }
            
            let user:PFUser = self.allUsers.objectAtIndex(0) as! PFUser
            print(user)
        })
        
        
        
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        if mediaType.isEqualToString(kUTTypeImage as NSString as String){
            
            image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            if imagePicker.sourceType == .Camera{
                
                UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
                //self.dismissViewControllerAnimated(true, completion: nil)
            }
            
            
        } else if mediaType.isEqualToString(kUTTypeMovie as NSString as String){
            
            videoFilePath = info[UIImagePickerControllerMediaURL]!.path
            if imagePicker.sourceType == .Camera{
                if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(videoFilePath){
                    
                    UISaveVideoAtPathToSavedPhotosAlbum(videoFilePath, nil, nil, nil)
                    // self.dismissViewControllerAnimated(true, completion: nil)
                }
            }
            
        }
        
        picker.dismissViewControllerAnimated(false, completion: nil)
       
        //presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
        //dismissViewControllerAnimated(true, completion: nil)
        
    }

    
    func uploadMessage() {
        
        let message = PFObject(className: "Messages")
        if videoFilePath == nil{
            //determine what device the phone is running one
            let newImage = resizeImage(image!, width: 750.0, height: 1334.0)
            
            let imageData = UIImagePNGRepresentation(newImage)
            let imageFile = PFFile(name: "image.png", data: imageData!)
            
            
            message["imageName"] = "Sexy Image"
            message["imageFile"] = imageFile
            message["fileType"] = "image"
        }
        else {
            print("YAY")
            let fileData = NSData(contentsOfFile: videoFilePath)
            let imageFile = PFFile(name: "video.mov", data: fileData!)
            message["fileType"] = "video"
            message["imageFile"] = imageFile
            
            
        }
        let user:PFUser = self.allUsers.objectAtIndex(0) as! PFUser
        recipients.addObject(user.objectId!)
        message.setObject(recipients, forKey: "recipientsIds")
        message.setObject(PFUser.currentUser()!, forKey: "senderId")
        message.setObject((PFUser.currentUser()?.username)!, forKey: "senderName")
        message.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
            if success{
                print("YAY")
                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                self.presentViewController(vc, animated: true, completion: nil)
                self.reset()
            }
        })
        
        
    }
    
    func reset() {
        image = nil
        recipients.removeAllObjects()
        videoFilePath = nil
    }
    
    func resizeImage(image:UIImage, width:CGFloat, height:CGFloat)->UIImage{
        
        let newSize = CGSizeMake(width, height)
        let newRectangle = CGRectMake(0, 0, width, height)
        UIGraphicsBeginImageContext(newSize)
        image.drawInRect(newRectangle)
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resizedImage
        
    }
    
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        self.dismissViewControllerAnimated(false, completion: nil)
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }



}
