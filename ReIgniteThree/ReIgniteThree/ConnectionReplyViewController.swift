//
//  ConnectionReplyViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/9/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class ConnectionReplyViewController: UIViewController {
    
    var spouseRelation:PFRelation = PFRelation()
    var recipient:NSMutableArray = []
    var spouseRecipient:NSMutableArray = []
    var spouse:NSArray! = []
    var sName:NSArray! = []
    var accept:String!
    var decline:String!
    var comments:String!
    var accepted:Bool = true
    var requestedUser:NSArray = []
    var currentUser:PFUser = PFUser()
    
 

    @IBOutlet var senderName: UILabel!
    @IBOutlet var commentField: UITextField!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        senderName.text = ""
        
        if let nav = navigationController?.navigationBar{
           
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.grayColor()]
            
        }
        
        let query = PFUser.query()
        query?.orderByDescending("createdAt")
        query?.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.requestedUser = objects!
                //print(self.allUsers)
                print(self.requestedUser.count)
                let user:PFUser = self.requestedUser.objectAtIndex(0) as!PFUser
                print(user.username)
                //current user relationship set
                self.spouseRelation = self.currentUser.relationForKey("spouseRelation")
                self.spouseRelation.addObject(user)
                //recipient sets relationship with current user
                //self.spouseRelation = user.relationForKey("spouseRelation")
                //self.spouseRelation.addObject(self.currentUser)
                self.currentUser.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                    
                    if success{
                        print("starting relations")
                        let initialConnection = PFObject(className: "AcceptedConnection")
                        //initialConnection["accept"] = "accepted"
                        //acceptedConnection["comment"] = self.comments
                        print(self.recipient)
                        //requested user object id
                        self.recipient.addObject(user.objectId!)
                        //current user object
                        
                        self.spouseRecipient.addObject(self.currentUser.objectId!)
                        print("Recipient=\(self.recipient)")
                        initialConnection.setObject(self.recipient, forKey: "recipientsIds")
                        initialConnection.setObject(self.spouseRecipient, forKey: "spouseRecipientsId")
                        initialConnection.setObject(PFUser.currentUser()!, forKey: "senderId")
                        initialConnection.setObject(self.currentUser.objectId!, forKey: "spouseSenderId")
                        initialConnection.setObject((PFUser.currentUser()?.username)!, forKey: "senderName")
                        //acceptedConnection.setObject(<#T##object: AnyObject##AnyObject#>, forKey: <#T##String#>)
                        
                        initialConnection.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
                            
                            if success{
                                //remove the connection request object
                                //self.selectedMessage = self.messages.objectAtIndex(0) as! PFObject
                                print("initial acceptance connection")
                                
                                let query = self.spouseRelation.query()
                                query.orderByAscending("username")
                                query.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
                                    
                                    if objects != nil {
                                        self.spouse = objects
                                        
                                        print(objects?.count)
                                        let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                                        print(user.username)
                                        print(user.objectId)
                                        
                                        self.recipient.addObject(user.objectId!)
                                        
                                        let query = PFQuery(className: "SpouseReply")
                                        query.whereKey("senderName", containsString: user.username)
                                        query.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
                                            
                                            if objects != nil {
                                                for object in objects!{
                                                    let name:String! = object.objectForKey("senderName") as! String
                                                    print(name)
                                                    if name == user.username {
                                                        
                                                        self.senderName.text = name
                                                        
                                                        
                                                        
                                                    } else {
                                                        print("no name")
                                                    }
                                                }
                                                
                                            } else {
                                                
                                            }

                                            
                                        })

                                        
                                    } else {
                                        
                                    }
                                })
                                
                                
                            } else{
                                print(error?.localizedDescription)
                            }
                        }
                        
                    } else{
                        print("boo no matching relation")
                    }
                    
                })

            }
        })
        
        spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
        currentUser = PFUser.currentUser()!
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        /*
        let query = spouseRelation.query()
        query.orderByAscending("username")
        query.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.spouse = objects!
                
            }
            print(objects?.count)
            let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
            print(user.username)
            print(user.objectId)
            
            self.recipient.addObject(user.objectId!)
            
            
            
            let query = PFQuery(className: "SpouseReply")
            query.whereKey("senderName", containsString: user.username)
            query.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) in
                
                if objects != nil {
                    for object in objects!{
                        let name:String! = object.objectForKey("senderName") as! String
                        print(name)
                        if name == user.username {
                            
                           self.senderName.text = name
                            
                        } else {
                            print("no name")
                        }
                    }
                    
                } else {
                    
                }
            }

        })
        
       */ 
    }



    @IBAction func acceptButtonTapped(sender: AnyObject) {
        acceptedConnection()
        homeView()
    }
    
    @IBAction func declineButtonTapped(sender: AnyObject) {
        declinedConnection()
        homeView()
    }
    


    func acceptedConnection(){
        
        //comments = commentField.text
    
        
        let query = PFUser.query()
        query?.orderByDescending("createdAt")
        
        query?.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.requestedUser = objects!
                //print(self.allUsers)
                print(self.requestedUser.count)
                let user:PFUser = self.requestedUser.objectAtIndex(0) as!PFUser
                print(user.username)
                //current user relationship set
                self.spouseRelation = self.currentUser.relationForKey("spouseRelation")
                self.spouseRelation.addObject(user)
                //recipient sets relationship with current user
                self.spouseRelation = user.relationForKey("spouseRelation")
                self.spouseRelation.addObject(self.currentUser)
                self.currentUser.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                    
                    if success{
                        print("Yay created matching relation")
                        let acceptedConnection = PFObject(className: "AcceptedConnection")
                        acceptedConnection["accept"] = "accepted"
                        //acceptedConnection["comment"] = self.comments
                        print(self.recipient)
                        //requested user object id
                        self.recipient.addObject(user.objectId!)
                        //current user object
                        
                        self.spouseRecipient.addObject(self.currentUser.objectId!)
                        print("Recipient=\(self.recipient)")
                        acceptedConnection.setObject(self.recipient, forKey: "recipientsIds")
                        acceptedConnection.setObject(self.spouseRecipient, forKey: "spouseRecipientsId")
                        acceptedConnection.setObject(PFUser.currentUser()!, forKey: "senderId")
                        acceptedConnection.setObject(self.currentUser.objectId!, forKey: "spouseSenderId")
                        acceptedConnection.setObject((PFUser.currentUser()?.username)!, forKey: "senderName")
                        //acceptedConnection.setObject(<#T##object: AnyObject##AnyObject#>, forKey: <#T##String#>)
                        
                        acceptedConnection.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
                            
                            if success{
                                //remove the connection request object
                                //self.selectedMessage = self.messages.objectAtIndex(0) as! PFObject
                                print("success acceptance connection")
                                
                            } else{
                                print(error?.localizedDescription)
                            }
                        }

                    } else{
                        print("boo no matching relation")
                    }
                    
                })
                
                
            }
        })
        
        
        
    }
    
    func declinedConnection(){
        let query = PFUser.query()
        query?.orderByDescending("createdAt")
        
        query?.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.requestedUser = objects!
                //print(self.allUsers)
                print(self.requestedUser.count)
                let user:PFUser = self.requestedUser.objectAtIndex(0) as!PFUser
                print(user.username)
                
                self.spouseRelation = self.currentUser.relationForKey("spouseRelation")
                self.spouseRelation.addObject(user)
                self.currentUser.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                    
                    if success{
                        print("Yay created matching relation")
                        //send decline
                        //remove relation
                        let declineConnection = PFObject(className: "DeclinedConnection")
                        declineConnection["decline"] = "not accepting currentl"
                        declineConnection["comment"] = self.comments
                        self.recipient.addObject(user.objectId!)
                        declineConnection.setObject(self.recipient, forKey: "recipientsIds")
                        declineConnection.setObject(PFUser.currentUser()!, forKey: "senderId")
                        declineConnection.setObject((PFUser.currentUser()?.username)!, forKey: "senderName")
                        
                        declineConnection.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
                            
                            if success{
                                //remove the connection request object
                                //self.selectedMessage = self.messages.objectAtIndex(0) as! PFObject
                                print("success: but no acceptance connection")
                                
                            } else{
                                print(error?.localizedDescription)
                            }
                        }

                    } else{
                        print("boo no matching relation and you dont want to be related")
                    }
                    
                })
                
                
            }
        })
    }
    
    func homeView() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }

}
