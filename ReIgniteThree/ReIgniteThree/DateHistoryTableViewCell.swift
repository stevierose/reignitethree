//
//  DateHistoryTableViewCell.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/14/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit

class DateHistoryTableViewCell: UITableViewCell {

    @IBOutlet var location: UILabel!
   
    @IBOutlet var time: UILabel!
    
    @IBOutlet var date: UILabel!
    
    @IBOutlet var attire: UILabel!
    
    @IBOutlet var placeLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var attireLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    
  

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
