//
//  ResetPasswordViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/15/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class ResetPasswordViewController: UIViewController {
    
    var email:String!

    @IBOutlet var emailField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()

        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
            
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
       
        
    }

    func isValidPassword(candidate:String)->Bool{
        
        let passwordRegex = "(?=.*[a-z])(?=.[A-Z])(?=.*\\d).{8,8}"
        
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluateWithObject(candidate)
        
    }
    
    func validate(YourEMailAddress: String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluateWithObject(YourEMailAddress)
    }
    
    @IBAction func resetPasswordButtonTapped(sender: AnyObject) {
        
        
         email = emailField.text
        if validate(email){
            
            PFUser.requestPasswordResetForEmailInBackground(email, block: { (success:Bool, error:NSError?) -> Void in
                
                
                if (error == nil) {
                    let success = UIAlertController(title: "Success", message: "Success! Check your email!", preferredStyle: .Alert)
                    let okButton = UIAlertAction(title: "OK", style: .Default, handler: nil)
                    success.addAction(okButton)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.presentViewController(success, animated: false, completion: nil)
                        
                        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
                        
                        let controllerNav = UINavigationController(rootViewController: controller)
                        
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.window?.rootViewController = controllerNav
                        
                    })
                }else {
                    let errormessage = error!.userInfo["error"] as! NSString
                    let error = UIAlertController(title: "Cannot complete request", message: errormessage as String, preferredStyle: .Alert)
                    let okButton = UIAlertAction(title: "OK", style: .Default, handler: nil)
                    error.addAction(okButton)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.presentViewController(error, animated: false, completion: nil)
                    })
                    
                }
            })

           
            
        } else {
            
            //send alert
            let alertController = UIAlertController(title: "Email has not been entered", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
            })
            alertController.addAction(defaultAction)
            presentViewController(alertController, animated: true, completion: nil)
        }
      
    }
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
        
    }
    
   
}
