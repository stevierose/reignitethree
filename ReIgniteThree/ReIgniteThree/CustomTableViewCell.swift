//
//  CustomTableViewCell.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/9/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!

    @IBOutlet var tableNames: UILabel!
    
    @IBOutlet var imageCell: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
