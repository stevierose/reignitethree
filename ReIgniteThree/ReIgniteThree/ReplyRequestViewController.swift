//
//  ReplyRequestViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/9/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse


class ReplyRequestViewController: UIViewController {
    
    var date:NSArray = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let nav = navigationController?.navigationBar{
           
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.grayColor()]
            
        }

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let dateQuery:PFQuery = PFQuery(className:"DateRequest")
        dateQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        dateQuery.orderByDescending("createdAt")
        dateQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                //we have a message
                self.date = objects!
                print(self.date.count)
                
                //let dates:PFObject = self.date.objectAtIndex(0) as! PFObject
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
